package me.develop.teachme.models

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.ColumnIgnore
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import me.develop.teachme.database.TeachmeDatabase

/**
 * Created by alex on 18.11.17.
 */
@Table(database = TeachmeDatabase::class, allFields = true, useBooleanGetterSetters = false)
class Dictionary : BaseModel() {

    @PrimaryKey(autoincrement = true)
    var id: Long = 0
    var name: String = ""
    var description: String = ""
    var image: String = ""

    var predefined: Boolean = false

    var wordsCount: Int = 0

    @ColumnIgnore
    var words: List<Word>? = null

}