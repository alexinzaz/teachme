package me.develop.teachme.models

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.ColumnIgnore
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import com.squareup.picasso.Callback
import me.develop.teachme.database.TeachmeDatabase
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Created by alex on 18.11.17.
 */
@Table(database = TeachmeDatabase::class, allFields = true)
class Word : BaseModel() {

    @PrimaryKey
     var id: Long = 0
     var english: String = ""
     var russian: String = ""
     var transcription: String = ""
     var frequency: Double = 0.toDouble()
     var complexity: Int = 0
     var part_of_speech: String = ""
     var en_sound: String = ""
     var ru_sound: String = ""
     var dictionaryId: Long = 0

    @ColumnIgnore
     var translate: List<String>? = null

    @ColumnIgnore
    var listener: Callback? = null


//    val translates: String
//        get() = TextUtils.join("/", translate)

//    fun playEng(ctx: Context) {
//        if (en_sound == null)
//            return
//
//        try {
//            val file = File(ctx.filesDir, english!! + ".mp3")
//
//            val out = FileOutputStream(file)
//
//            out.write(Base64.decode(en_sound, 0))
//
//            out.close()
//
//            val filePlayer = MediaPlayer.create(ctx, Uri.parse(file.absolutePath))
//
//            filePlayer.setOnCompletionListener {
//                file.delete()
//                filePlayer.release()
//            }
//            filePlayer.start()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//
//    }
//
//    fun playRu(ctx: Context) {
//        if (ru_sound == null)
//            return
//
//        try {
//            val file = File(ctx.filesDir, russian!! + ".mp3")
//
//            val out = FileOutputStream(file)
//
//            out.write(Base64.decode(ru_sound, 0))
//
//            out.close()
//
//            val filePlayer = MediaPlayer.create(ctx, Uri.parse(file.absolutePath))
//
//            filePlayer.setOnCompletionListener {
//                file.delete()
//                filePlayer.release()
//            }
//            filePlayer.start()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//
//    }
//
//    fun playAllSound(ctx: Context) {
//        Log.d("ZAZ", "start play sound")
//        if (en_sound.isEmpty()) {
//            listener?.onSuccess()
//            Log.d("ZAZ", "no english")
//            return
//        }
//        try {
//            Log.d("ZAZ", "play_en")
//            val file = File(ctx.filesDir, english!! + ".mp3")
//
//            val out = FileOutputStream(file)
//
//            out.write(Base64.decode(en_sound, 0))
//
//            out.close()
//
//            val filePlayer = MediaPlayer.create(ctx, Uri.parse(file.absolutePath))
//
//            filePlayer.setOnCompletionListener {
//                file.delete()
//                filePlayer.release()
//                try {
//                    Thread.sleep(1000)
//                } catch (e: InterruptedException) {
//                    e.printStackTrace()
//                }
//
//                if (ru_sound.length == 0) {
//                    Log.d("ZAZ", "no russian")
//                    listener?.onSuccess()
//                } else {
//                    try {
//                        Log.d("ZAZ", "play_ru")
//                        val file = File(ctx.filesDir, russian!! + ".mp3")
//
//                        val out = FileOutputStream(file)
//
//                        out.write(Base64.decode(ru_sound, 0))
//
//                        out.close()
//
//                        val filePlayer = MediaPlayer.create(ctx, Uri.parse(file.absolutePath))
//
//                        filePlayer.setOnCompletionListener {
//                            file.delete()
//                            filePlayer.release()
//                            Log.d("ZAZ", "ru_complite")
//                            listener?.onSuccess()
//                        }
//                        filePlayer.start()
//                    } catch (e: IOException) {
//                        e.printStackTrace()
//                    }
//
//                }
//            }
//            filePlayer.start()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//
//    }
//
//
//    fun setOnCompletionListener(listener: Callback) {
//        this.listener = listener
//    }
//
//    fun setTranscription(transcription: String) {
//        this.transcription = transcription
//    }

}