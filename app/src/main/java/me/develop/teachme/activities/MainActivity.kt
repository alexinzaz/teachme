package me.develop.teachme.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import me.develop.teachme.R
import me.develop.teachme.fragments.MyDictionariesFragment
import me.develop.teachme.models.Dictionary

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val onBottomTabClickListener: View.OnClickListener = View.OnClickListener { v ->

            img_dictionary.setImageResource(R.drawable.ic_dictionaries)
            img_favourite.setImageResource(R.drawable.ic_favourite)
            img_statistic.setImageResource(R.drawable.ic_statistics)
            img_teach.setImageResource(R.drawable.ic_teach)

            tv_dictionary.setTextColor(ContextCompat.getColor(this, R.color.bottomBarNormalTextColor))
            tv_favourite.setTextColor(ContextCompat.getColor(this, R.color.bottomBarNormalTextColor))
            tv_statistic.setTextColor(ContextCompat.getColor(this, R.color.bottomBarNormalTextColor))
            tv_teach.setTextColor(ContextCompat.getColor(this, R.color.bottomBarNormalTextColor))

            when (v.id) {

                R.id.rl_dictionaries -> {
                    img_dictionary.setImageResource(R.drawable.ic_dictionaries_pressed)
                    tv_dictionary.setTextColor(ContextCompat.getColor(this, R.color.bottomBarPressedTextColor))
                }
                R.id.rl_statistic -> {
                    img_statistic.setImageResource(R.drawable.ic_statistics_pressed)
                    tv_statistic.setTextColor(ContextCompat.getColor(this, R.color.bottomBarPressedTextColor))
//                    supportFragmentManager
//                            .beginTransaction()
//                            .hide(homePageFragment)
//                            .show(favouriteFragment)
//                            .commit()
                }
                R.id.rl_teach -> {
                    img_teach.setImageResource(R.drawable.ic_teach_pressed)
                    tv_teach.setTextColor(ContextCompat.getColor(this, R.color.bottomBarPressedTextColor))
                }
                R.id.rl_favourite -> {
                    img_favourite.setImageResource(R.drawable.ic_favourite_pressed)
                    tv_favourite.setTextColor(ContextCompat.getColor(this, R.color.bottomBarPressedTextColor))
                }
            }
        }

        rl_dictionaries.setOnClickListener(onBottomTabClickListener)
        rl_statistic.setOnClickListener(onBottomTabClickListener)
        rl_teach.setOnClickListener(onBottomTabClickListener)
        rl_favourite.setOnClickListener(onBottomTabClickListener)

        val dic = Dictionary()
        dic.id =1
        dic.name = "test 1"
        dic.save()

        val dic1 = Dictionary()
        dic1.id =2
        dic1.name = "test 2"
        dic1.save()

        val dic3 = Dictionary()
        dic3.id =3
        dic3.name = "test 2"
        dic3.save()
        supportFragmentManager
                .beginTransaction()
                .add(R.id.container, MyDictionariesFragment.newInstance(), "MyDictionaries")
                .commit()

    }
}
