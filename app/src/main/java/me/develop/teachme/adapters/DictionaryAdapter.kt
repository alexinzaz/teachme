package me.develop.teachme.adapters

import android.support.v7.widget.RecyclerView
import me.develop.teachme.models.Dictionary
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.TextView
import me.develop.teachme.R
import java.util.*

/**
 * Created by alex on 18.11.17.
 */
class DictionaryAdapter(private var dictionaries: List<Dictionary>, private val context: Context) : RecyclerView.Adapter<DictionaryAdapter.ViewHolder>() {

    override fun getItemCount(): Int = dictionaries.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dictionary = dictionaries[position]

        holder.dictionaryName.text = dictionary.name

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {

        val inflater = LayoutInflater.from(context)

        return ViewHolder(inflater.inflate(R.layout.dictionary_list_item, parent, false))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val dictionaryName: TextView = itemView.findViewById<TextView>(R.id.text_dictionary_name) as TextView

    }

}