package me.develop.teachme.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.develop.teachme.R
import me.develop.teachme.models.Word

/**
 * Created by alex on 18.11.17.
 */
class WordAdapter(private var words: List<Word>, private val context: Context) : RecyclerView.Adapter<WordAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val word = words[position]

        holder.english.text = word.english
        holder.transcription.text = word.transcription
        holder.translate.text = TextUtils.join(",", word.translate)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)

        return ViewHolder(inflater.inflate(R.layout.word_list_item, parent, false))
    }

    override fun getItemCount(): Int = words.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val english: TextView = itemView.findViewById<TextView>(R.id.text_word) as TextView
        val translate: TextView = itemView.findViewById<TextView>(R.id.text_translate) as TextView
        val transcription: TextView = itemView.findViewById<TextView>(R.id.text_transcription) as TextView

    }
}