package me.develop.teachme.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import me.develop.teachme.R


/**
 * A simple [Fragment] subclass.
 */
class DictionaryFragment : Fragment() {

    companion object {
        fun newInstance(): DictionaryFragment {

            return DictionaryFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_dictionary, container, false)
    }

}
