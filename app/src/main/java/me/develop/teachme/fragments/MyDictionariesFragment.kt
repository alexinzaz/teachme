package me.develop.teachme.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.raizlabs.android.dbflow.kotlinextensions.from
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.kotlinextensions.select
import kotlinx.android.synthetic.main.fragment_my_dictionaries.*
import me.develop.teachme.R
import me.develop.teachme.adapters.DictionaryAdapter
import me.develop.teachme.models.Dictionary
import me.develop.teachme.views.RecyclerItemClickListener


class MyDictionariesFragment : Fragment() {

    companion object {
        fun newInstance(): MyDictionariesFragment = MyDictionariesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_my_dictionaries, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        list_dictionaries.layoutManager = LinearLayoutManager(activity)

        list_dictionaries.addOnItemTouchListener(RecyclerItemClickListener(activity, list_dictionaries, object : RecyclerItemClickListener.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }

            override fun onLongItemClick(view: View, position: Int) {
            }
        }))

        loadDictionaries()

        button_save.setOnClickListener({

            val dictionary = Dictionary()

            dictionary.name = edit_text_dictionary_name.text.toString()
            dictionary.description = edit_text_dictionary_description.text.toString()
            dictionary.save()

            constraint_no_dictionaries.visibility = View.GONE

            loadDictionaries()
        })

        button_cancel.setOnClickListener({
            constraint_add_dictionary.visibility = View.GONE
        })

        super.onViewCreated(view, savedInstanceState)
    }

    private fun loadDictionaries() {

        (select from Dictionary::class).async() list { transaction, list ->

            list_dictionaries.swapAdapter(DictionaryAdapter(list, activity), false)

            if (list.isEmpty()) {
                constraint_no_dictionaries.visibility = View.VISIBLE
            } else {
                constraint_no_dictionaries.visibility = View.GONE
            }

        }
    }

}
