package me.develop.teachme

import android.app.Application
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import me.develop.teachme.api.ApiFactory
import me.develop.teachme.api.ApiTeachme
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Created by alex on 18.11.17.
 */
class TeachmeApplication : Application() {

    companion object {
        var service: ApiTeachme? = null

    }

    override fun onCreate() {
        super.onCreate()

//        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
//                .setDefaultFontPath("Roboto-Regular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        )

//        service = ApiFactory().getService()

        FlowManager.init(FlowConfig.Builder(this).openDatabasesOnInit(true).build())


    }
}