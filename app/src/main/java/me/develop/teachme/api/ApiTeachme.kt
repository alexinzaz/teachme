package me.develop.teachme.api

import io.reactivex.Observable
import me.develop.teachme.models.Dictionary
import me.develop.teachme.models.Word
import retrofit2.http.*


/**
 * Created by alex on 18.11.17.
 */
interface ApiTeachme {

    @GET("words/{word}")
    fun getWord(@Path("word") word: String): Observable<Word>

    @GET("allwords/{word}")
    fun getWordsByCoincidence(@Path("word") word: String): Observable<List<String>>

    @GET("dictionary/all")
    fun getAllDictionary(): Observable<List<Dictionary>>

    @GET("dictionary/{dictionary}")
    fun getDictionary(@Path("dictionary") dictionaryId: Long): Observable<List<String>>

    @FormUrlEncoded
    @POST("dictionary/words/add")
    fun addWordInDictionary(@Field("payment") dictionary_id: String, @Field("payment") word_id: String): Observable<Void>

}