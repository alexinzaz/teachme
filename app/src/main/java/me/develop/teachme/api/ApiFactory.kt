package me.develop.teachme.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by alex on 18.11.17.
 */
class ApiFactory {

    fun getService(): ApiTeachme = getRetrofit().create<ApiTeachme>(ApiTeachme::class.java)

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("http://192.168.1.237:8080/silex/web/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient())
                .build()

    }

}