package me.develop.teachme.database

import com.raizlabs.android.dbflow.annotation.Database
import me.develop.teachme.database.TeachmeDatabase.NAME
import me.develop.teachme.database.TeachmeDatabase.VERSION

/**
 * Created by alex on 18.11.17.
 */
@Database(name = NAME, version = VERSION, generatedClassSeparator = "_")
object TeachmeDatabase {
    const val NAME: String = "Teachme"
    const val VERSION: Int = 1
}